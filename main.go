package main

import (
	"fmt"
	"net/http"
	"log"
)

const webContent = "Hello World! 11 22"

func main() {
        fmt.Println("hello world")
	http.HandleFunc("/", helloHandler)
	log.Fatal(http.ListenAndServe(":80", nil))
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, webContent)
	fmt.Println("hello server")
}
