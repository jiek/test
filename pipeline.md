### 使用rancher-pipe
- 关联gitlab
  - 使用提供的back url生成id和secret
- 在项目目录中必须有下列文件
  - Dockerfile：打包image文件
  - deployment.yaml ：项目部署到k8s的配置文件
  - .rancher-pipeline.yaml：流程控制文件

### pipe自带流程
1. git clone
2. build 和test
3. publish
4. deployment

### 可以修改的点 
 - build ，test执行脚本
 - image 的仓库
 - 部署文件deployment.yaml
 - 添加更多的流程

